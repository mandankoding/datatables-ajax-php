<?php
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "stokBarang";

$connect = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

$tampil = mysqli_query($connect, "SELECT * FROM  tb_stok_barang ORDER BY id DESC");
$result = array();

while ($row = mysqli_fetch_array($tampil)){
    array_push($result, array(
        'nama_barang' => $row[1],
        'harga_barang' =>$row[2],
        'stok' =>$row[3],
        'supplies' =>$row[4]
    ));

}
echo json_encode(array("data" => $result));
